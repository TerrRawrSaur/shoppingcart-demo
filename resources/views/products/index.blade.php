@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            @foreach($products as $product)
            <div class="col-xs-12 col-sm-6 col-md-3 item">
                <a href="{{ route('products.show', $product->id) }}">{{ $product->name }} – ${{$product->price }}</a>
                <img class="img-responsive" src="http://placehold.it/150x120">
            </div>
            @endforeach
        </div>
    </div>
@endsection
