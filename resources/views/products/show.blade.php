@extends('layouts.master')

@section('content')

    <div class="container">
        <h1>{{ $product->name }}</h1>
        <p>${{ $product->price }}</p>
        <p>{{ $product->description }}</p>

        @if(Auth::check())
            {!! Form::open(['route' => 'cart.add', 'class' => 'form-horizontal', 'files' => true]) !!}
                {!! Form::hidden('product_id', $product->id) !!}
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Add To Cart</button>
                </div>
            {!! Form::close() !!}
        @endif
    </div>
@endsection
