@extends('layouts.master')

@section('content')
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1>Payment Received</h1>
            <p>Thankyou! Your payment has been received</p>
            <p>Total: {{ $order->price }}</p>
            <p>Order Reference: {{ $order->payment_reference }}</p>
        </div>
    </div>
@endsection
