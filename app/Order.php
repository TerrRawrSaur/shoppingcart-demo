<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products')->withPivot('quantity', 'price')->withTimestamps();
    }

    public function subtotal()
    {
        $products = $this->products;
        $subtotal = 0;
        foreach($products as $product) {
            $subtotal += ($product->pivot->quantity * $product->pivot->price);
        }
        return $subtotal;
    }
}
